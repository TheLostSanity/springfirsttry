## Simple web application made for educational purpose
### In order to run this app
Create local mysql database with 
```
create database %dbname%;
create user '%username%'@'localhost' identified by '%password%';
grant all on %dbname%.* to '%username%'@'localhost';
```
In [application.properties](src/main/resources/application.properties) set fields
```
spring.datasource.url=%jdbcConnectionString%
spring.datasource.username=%username%
spring.datasource.password=%password%
```

## WIP
Divide viewmodels into "Create" and "Edit" viewmodels\
Create error pages\
Add message attachments\
Cleanup code