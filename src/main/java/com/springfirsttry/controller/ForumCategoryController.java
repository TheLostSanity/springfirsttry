package com.springfirsttry.controller;

import com.springfirsttry.model.ForumCategory;
import com.springfirsttry.repository.ForumCategoryRepository;
import com.springfirsttry.viewmodel.ForumCategoryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping("/categories")
public class ForumCategoryController {

    private final ForumCategoryRepository forumCategoryRepository;

    @Autowired
    public ForumCategoryController(ForumCategoryRepository forumCategoryRepository) {
        this.forumCategoryRepository = forumCategoryRepository;
    }

    @PreAuthorize("permitAll()")
    @GetMapping({"", "/index"})
    public String getIndex(Model model, HttpServletRequest request) {
        model.addAttribute("model", forumCategoryRepository.findAll());
        model.addAttribute("isAdmin", request.isUserInRole("ROLE_ADMIN"));
        return "categories/index";
    }

    @GetMapping("/create")
    public String getCreate(Model model) {
        model.addAttribute("model", new ForumCategoryModel());
        return "categories/create";
    }

    @PostMapping("/create")
    public String postCreate(@ModelAttribute("model") @Valid ForumCategoryModel model, BindingResult bindingResult, Model m) {
        if (bindingResult.hasErrors()) {
            m.addAttribute("model", model);
            return "categories/create";
        }
        forumCategoryRepository.createAndSave(model);
        return "redirect:index";
    }

    @GetMapping("/edit/{id}")
    public String getEdit(@PathVariable Integer id, Model model) {
        Optional<ForumCategory> f = forumCategoryRepository.findById(id);
        if (f.isPresent()) {
            ForumCategoryModel forumCategoryModel = new ForumCategoryModel();
            forumCategoryModel.setCategoryName(f.get().getCategoryName());
            model.addAttribute("model", forumCategoryModel);
            return "categories/edit";
        }
        //todo
        return "";
    }

    @PostMapping("/edit/{id}")
    public String postEdit(@PathVariable Integer id, @ModelAttribute("model") @Valid ForumCategoryModel model, BindingResult bindingResult, Model m) {
        if (bindingResult.hasErrors()) {
            m.addAttribute("model", model);
            return "categories/edit";
        }
        Optional<ForumCategory> optForumCategory = forumCategoryRepository.findById(id);
        if (optForumCategory.isPresent()) {
            ForumCategory forumCategory = optForumCategory.get();
            if (forumCategory.getId().equals(id)) {
                forumCategoryRepository.editAndSave(forumCategory, model);
                return "redirect:../index";
            }
        }
        return "categories/edit";
    }

    @GetMapping("/delete/{id}")
    public String getDelete(@PathVariable Integer id, Model model) {
        Optional<ForumCategory> f = forumCategoryRepository.findById(id);
        if (f.isPresent()) {
            ForumCategoryModel forumCategoryModel = new ForumCategoryModel();
            forumCategoryModel.setCategoryName(f.get().getCategoryName());
            model.addAttribute("model", forumCategoryModel);
            return "categories/delete";
        }
        //todo
        return "";
    }

    @PostMapping("/delete/{id}")
    public String postDelete(@PathVariable Integer id) {
        Optional<ForumCategory> f = forumCategoryRepository.findById(id);
        if (f.isPresent()) {
            ForumCategory forumCategory = f.get();
            if (forumCategory.getId().equals(id)) {
                forumCategoryRepository.delete(forumCategory);
                return "redirect:../index";
            }
        }
        return "categories/delete";
    }
}
