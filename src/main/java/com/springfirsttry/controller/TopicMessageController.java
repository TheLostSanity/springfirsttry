package com.springfirsttry.controller;

import com.springfirsttry.model.Topic;
import com.springfirsttry.model.TopicMessage;
import com.springfirsttry.model.User;
import com.springfirsttry.repository.TopicMessageRepository;
import com.springfirsttry.repository.TopicRepository;
import com.springfirsttry.service.UserService;
import com.springfirsttry.viewmodel.TopicMessageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/message")
public class TopicMessageController {

    private final TopicRepository topicRepository;
    private final TopicMessageRepository topicMessageRepository;
    private final UserService userService;

    @Autowired
    public TopicMessageController(
            TopicRepository topicRepository,
            TopicMessageRepository topicMessageRepository,
            UserService userService
    ) {
        this.topicRepository = topicRepository;
        this.topicMessageRepository = topicMessageRepository;
        this.userService = userService;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/{id}/create")
    public String getCreate(@PathVariable Integer id, Model model) {
        Optional<Topic> optTopic = topicRepository.findById(id);
        if (optTopic.isPresent()) {
            TopicMessageModel message = new TopicMessageModel();
            model.addAttribute("model", message);
            return "message/create";
        }
        //todo
        return "";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping("/{id}/create")
    public String postCreate(
            @PathVariable Integer id,
            @ModelAttribute("model") @Valid TopicMessageModel model,
            BindingResult bindingResult,
            Model m,
            HttpServletRequest request
    ) {
        if (bindingResult.hasErrors()) {
            m.addAttribute("model", model);
            return "message/create";
        }

        User user = (User) userService.loadUserByUsername(request.getUserPrincipal().getName());
        TopicMessage topicMessage = topicMessageRepository.createAndSave(model, user, id);
        if (topicMessage == null) {
            //todo
            return "";
        }
        return "redirect:../../topic/" + topicMessage.getMessageTopic().getId();
    }

    @GetMapping("/{id}/edit")
    public String getEdit(@PathVariable Integer id, Model model, HttpServletRequest request) {
        Optional<TopicMessage> optTopicMessage = topicMessageRepository.findById(id);
        if (optTopicMessage.isPresent()) {
            TopicMessage message = optTopicMessage.get();
            boolean canEditTopicMessage = userService.canEditTopicMessage(message, request.getUserPrincipal()) || request.isUserInRole("ROLE_ADMIN");
            if (!canEditTopicMessage) {
                return "redirect:../../topic/" + message.getMessageTopic().getId();
            }
            TopicMessageModel topicModel = new TopicMessageModel();
            topicModel.setText(message.getText());
            topicModel.setTopicId(message.getMessageTopic().getId());
            model.addAttribute("model", topicModel);
            return "message/edit";
        }
        //todo
        return "";
    }

    @PostMapping("/{id}/edit")
    public String postEdit(
            @PathVariable Integer id,
            @ModelAttribute("model") @Valid TopicMessageModel model,
            BindingResult bindingResult,
            Model m,
            HttpServletRequest request
    ) {
        Optional<TopicMessage> optTopicMessage = topicMessageRepository.findById(id);
        if (optTopicMessage.isPresent()) {
            TopicMessage message = optTopicMessage.get();
            boolean canEditTopicMessage = userService.canEditTopicMessage(message, request.getUserPrincipal()) || request.isUserInRole("ROLE_ADMIN");
            if (!canEditTopicMessage) {
                return "redirect:../../topic/" + message.getMessageTopic().getId();
            }
            if (bindingResult.hasErrors()) {
                model.setTopicId(message.getMessageTopic().getId());
                m.addAttribute("model", model);
                return "topic/edit";
            }
            if (message.getId().equals(id)) {
                TopicMessage savedMessage = topicMessageRepository.editAndSave(message, model);
                return "redirect:../../topic/" + savedMessage.getMessageTopic().getId();
            }
        }
        //todo
        return "forum/edit";
    }

    @GetMapping("/{id}/delete")
    public String getDelete(@PathVariable Integer id, Model model, HttpServletRequest request) {
        Optional<TopicMessage> message = topicMessageRepository.findById(id);
        if (message.isPresent()) {
            boolean canEditTopicMessage = userService.canEditTopicMessage(message.get(), request.getUserPrincipal()) || request.isUserInRole("ROLE_ADMIN");
            if (!canEditTopicMessage) {
                return "redirect:../../topic/" + message.get().getMessageTopic().getId();
            }
            model.addAttribute("model", message.get());
            return "message/delete";
        }
        //todo
        return "";
    }

    @PostMapping("/{id}/delete")
    public String postDelete(@PathVariable Integer id, HttpServletRequest request) {
        Optional<TopicMessage> message = topicMessageRepository.findById(id);
        if (message.isPresent()) {
            boolean canEditTopicMessage = userService.canEditTopicMessage(message.get(), request.getUserPrincipal()) || request.isUserInRole("ROLE_ADMIN");
            if (canEditTopicMessage) {
                topicMessageRepository.delete(message.get());
            }
            return "redirect:../../topic/" + message.get().getMessageTopic().getId();
        }
        //todo
        return "topic/delete";
    }
}
