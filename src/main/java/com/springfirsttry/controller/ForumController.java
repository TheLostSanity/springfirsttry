package com.springfirsttry.controller;

import com.springfirsttry.model.Forum;
import com.springfirsttry.repository.ForumCategoryRepository;
import com.springfirsttry.repository.ForumRepository;
import com.springfirsttry.viewmodel.ForumModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@PreAuthorize("hasRole('ROLE_ADMIN')")
@RequestMapping("/forum")
public class ForumController {

    private final ForumRepository forumRepository;
    private final ForumCategoryRepository forumCategoryRepository;

    @Autowired
    public ForumController(ForumRepository forumRepository, ForumCategoryRepository forumCategoryRepository) {
        this.forumRepository = forumRepository;
        this.forumCategoryRepository = forumCategoryRepository;
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/{id}")
    public String getIndex(@PathVariable Integer id, Model model, HttpServletRequest request) {
        Optional<Forum> optForum = forumRepository.findById(id);
        if (optForum.isPresent()) {
            model.addAttribute("model", optForum.get());
            model.addAttribute("isAdmin", request.isUserInRole("ROLE_ADMIN"));
            return "forum/index";
        }
        //todo
        return "";
    }

    @GetMapping("/create")
    public String getCreate(Model model) {
        model.addAttribute("model", new ForumModel());
        model.addAttribute("categories", forumCategoryRepository.findAll());
        return "forum/create";
    }

    @PostMapping("/create")
    public String postCreate(@ModelAttribute("model") @Valid ForumModel model, BindingResult bindingResult, Model m) {
        if (bindingResult.hasErrors()) {
            m.addAttribute("model", model);
            m.addAttribute("categories", forumCategoryRepository.findAll());
            return "forum/create";
        }
        Forum forum = forumRepository.createAndSave(model);
        return "redirect:" + forum.getId();
    }

    @GetMapping("/edit/{id}")
    public String getEdit(@PathVariable Integer id, Model model) {
        Optional<Forum> f = forumRepository.findById(id);
        if (f.isPresent()) {
            ForumModel forumModel = new ForumModel();
            forumModel.setForumName(f.get().getForumName());
            forumModel.setDescription(f.get().getDescription());
            forumModel.setCategoryId(f.get().getForumCategory().getId());
            model.addAttribute("model", forumModel);
            model.addAttribute("categories", forumCategoryRepository.findAll());
            return "forum/edit";
        }
        //todo
        return "";
    }

    @PostMapping("/edit/{id}")
    public String postEdit(@PathVariable Integer id, @ModelAttribute("model") @Valid ForumModel model, BindingResult bindingResult, Model m) {
        if (bindingResult.hasErrors()) {
            m.addAttribute("model", model);
            m.addAttribute("categories", forumCategoryRepository.findAll());
            return "forum/edit";
        }
        Optional<Forum> optForum = forumRepository.findById(id);
        if (optForum.isPresent()) {
            Forum forum = optForum.get();
            if (forum.getId().equals(id)) {
                forumRepository.editAndSave(forum, model);
                return "redirect:../../categories";
            }
        }
        return "forum/edit";
    }

    @GetMapping("/delete/{id}")
    public String getDelete(@PathVariable Integer id, Model model) {
        Optional<Forum> f = forumRepository.findById(id);
        if (f.isPresent()) {
            ForumModel forumModel = new ForumModel();
            forumModel.setForumName(f.get().getForumName());
            forumModel.setDescription(f.get().getDescription());
            forumModel.setForumCategory(f.get().getForumCategory());
            model.addAttribute("model", forumModel);
            return "forum/delete";
        }
        //todo
        return "";
    }

    @PostMapping("/delete/{id}")
    public String postDelete(@PathVariable Integer id) {
        Optional<Forum> f = forumRepository.findById(id);
        if (f.isPresent()) {
            Forum forum = f.get();
            if (forum.getId().equals(id)) {
                forumRepository.delete(forum);
                return "redirect:../../categories";
            }
        }
        return "forum/delete";
    }
}
