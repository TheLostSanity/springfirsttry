package com.springfirsttry.controller;

import com.springfirsttry.model.Forum;
import com.springfirsttry.model.Topic;
import com.springfirsttry.model.TopicMessage;
import com.springfirsttry.model.User;
import com.springfirsttry.repository.ForumRepository;
import com.springfirsttry.repository.TopicMessageRepository;
import com.springfirsttry.repository.TopicRepository;
import com.springfirsttry.service.UserService;
import com.springfirsttry.viewmodel.TopicModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;

@Controller
@PreAuthorize("hasRole('ROLE_USER')")
@RequestMapping("/topic")
public class TopicController {

    private final TopicRepository topicRepository;
    private final ForumRepository forumRepository;
    private final TopicMessageRepository topicMessageRepository;
    private final UserService userService;
//    private final TopicRepositoryCustom topicRepositoryCustom;

    @Autowired
    public TopicController(
            TopicRepository topicRepository,
            ForumRepository forumRepository,
            TopicMessageRepository topicMessageRepository,
            UserService userService
//            TopicRepositoryCustom topicRepositoryCustom
    ) {
        this.topicRepository = topicRepository;
        this.forumRepository = forumRepository;
        this.topicMessageRepository = topicMessageRepository;
        this.userService = userService;
//        this.topicRepositoryCustom = topicRepositoryCustom;
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/{id}")
    public String getIndex(@PathVariable Integer id, Model model, HttpServletRequest request) {
        Optional<Topic> optTopic = topicRepository.findById(id);
        if (optTopic.isPresent()) {
            model.addAttribute("model", optTopic.get());
            model.addAttribute(
                    "canEditTopic",
                    userService.canEditTopic(optTopic.get(), request.getUserPrincipal())
                            || request.isUserInRole("ROLE_ADMIN")
            );
            model.addAttribute("isAdmin", request.isUserInRole("ROLE_ADMIN"));
            try {
                model.addAttribute("currUsername", request.getUserPrincipal().getName());
            } catch (NullPointerException ex) {
                model.addAttribute("currUsername", null);
            }
            return "topic/index";
        }
        //todo
        return "redirect:categories";
    }

    @GetMapping("/create/{id}")
    public String getCreate(@PathVariable Integer id, Model model) {
        Optional<Forum> optForum = forumRepository.findById(id);
        if (optForum.isPresent()) {
            TopicModel topic = new TopicModel();
            topic.setForumId(id);
            model.addAttribute("model", topic);
            return "topic/create";
        }
        //todo
        return "";
    }

    @PostMapping("/create/{id}")
    public String postCreate(
            @PathVariable Integer id,
            @ModelAttribute("model") @Valid TopicModel model,
            BindingResult bindingResult,
            Model m,
            HttpServletRequest request
    ) {
        if (bindingResult.hasErrors()) {
            model.setForumId(id);
            m.addAttribute("model", model);
            return "topic/create";
        }
        User user = (User) userService.loadUserByUsername(request.getUserPrincipal().getName());
        Topic topic = topicRepository.createAndSave(model, user, id);
        Date now = Date.from(Instant.now());
        /*Topic topic = new Topic();
        topic.setTopicName(model.getTopicName());
        topic.setCreated(now);
        topic.setTopicForum(forumRepository.findById(id).get());
        topic.setCreator(user);
        topicRepository.save(topic);*/

        TopicMessage topicMessage = new TopicMessage();
        topicMessage.setText(model.getMessage());
        topicMessage.setCreated(now);
        topicMessage.setModified(now);
        topicMessage.setMessageTopic(topic);
        topicMessage.setCreator(user);
        topicMessageRepository.save(topicMessage);

        return "redirect:../" + topic.getId();
    }

    @GetMapping("/{id}/edit")
    public String getEdit(@PathVariable Integer id, Model model, HttpServletRequest request) {
        Optional<Topic> t = topicRepository.findById(id);
        if (t.isPresent()) {
            boolean canEditTopic = userService.canEditTopic(t.get(), request.getUserPrincipal()) || request.isUserInRole("ROLE_ADMIN");
            if (!canEditTopic) {
                return "redirect:../" + id;
            }
            TopicModel topicModel = new TopicModel();
            topicModel.setForumId(t.get().getTopicForum().getId());
            topicModel.setTopicName(t.get().getTopicName());
            model.addAttribute("model", topicModel);
            return "topic/edit";
        }
        //todo
        return "";
    }

    @PostMapping("/{id}/edit")
    public String postEdit(
            @PathVariable Integer id,
            @ModelAttribute("model") @Valid TopicModel model,
            BindingResult bindingResult,
            Model m,
            HttpServletRequest request
    ) {
        Optional<Topic> t = topicRepository.findById(id);
        if (t.isPresent()) {
            boolean canEditTopic = userService.canEditTopic(t.get(), request.getUserPrincipal()) || request.isUserInRole("ROLE_ADMIN");
            if (!canEditTopic) {
                return "redirect:../" + id;
            }
            if (bindingResult.hasErrors()) {
                model.setForumId(t.get().getTopicForum().getId());
                m.addAttribute("model", model);
                return "topic/edit";
            }
            Topic topic = t.get();
            if (topic.getId().equals(id)) {
                topicRepository.editAndSave(topic, model);
                return "redirect:../" + id;
            }
        }
        //todo
        return "forum/edit";
    }

    @GetMapping("/{id}/delete")
    public String getDelete(@PathVariable Integer id, Model model, HttpServletRequest request) {
        Optional<Topic> t = topicRepository.findById(id);
        if (t.isPresent()) {
            boolean canEditTopic = userService.canEditTopic(t.get(), request.getUserPrincipal()) || request.isUserInRole("ROLE_ADMIN");
            if (!canEditTopic) {
                return "redirect:../" + id;
            }
            model.addAttribute("model", t.get());
            return "topic/delete";
        }
        //todo
        return "";
    }

    @PostMapping("/{id}/delete")
    public String postDelete(@PathVariable Integer id, HttpServletRequest request) {
        Optional<Topic> t = topicRepository.findById(id);
        if (t.isPresent()) {
            boolean canEditTopic = userService.canEditTopic(t.get(), request.getUserPrincipal()) || request.isUserInRole("ROLE_ADMIN");
            if (!canEditTopic) {
                return "redirect:../" + id;
            }
            if (t.get().getId().equals(id)) {
                topicRepository.delete(t.get());
                return "redirect:../../forum/" + t.get().getTopicForum().getId();
            }
        }
        return "topic/delete";
    }
}
