package com.springfirsttry.controller;

import com.springfirsttry.model.User;
import com.springfirsttry.service.UserService;
import com.springfirsttry.viewmodel.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class AuthController {

    @Autowired
    private UserService userService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private AuthenticationManager authManager;

    @GetMapping("/registration")
    public String getRegistration(Model model) {
        model.addAttribute("model", new User());
        return "registration";
    }

    @PostMapping("/registration")
    public String postRegistration(@ModelAttribute("model") @Valid User model, BindingResult bindingResult, Model m) {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        if (!model.getPassword().equals(model.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "error.model", "Passwords do not match");
            return "registration";
        }
        if (!userService.saveUser(model)) {
            bindingResult.rejectValue("username", "error.model", "User with this username already exists");
            return "registration";
        }
        return "redirect:/";
    }

    @GetMapping("/login")
    public String getLogin(Model model) {
        model.addAttribute("model", new UserModel());
        return "login";
    }

    @PostMapping("/login")
    public String postLogin(@ModelAttribute("model") @Valid UserModel model, BindingResult bindingResult, Model m, HttpServletRequest request) throws ServletException {
        if (bindingResult.hasErrors()) {
            return "login";
        }
        UserDetails userDetails = userService.loadUserByUsername(model.getUsername());
        if (!userDetails.getUsername().equals(model.getUsername())) {
            bindingResult.rejectValue("password", "error.model", "No user with such username");
            return "login";
        }
        if (!bCryptPasswordEncoder.matches(model.getPassword(), userDetails.getPassword())) {
            bindingResult.rejectValue("username", "error.model", "Passwords do not match");
            return "login";
        }
        request.login(model.getUsername(), model.getPassword());
        return "redirect:/";
    }

    @GetMapping("/logout")
    public String getLogout(HttpServletRequest request) throws ServletException {
        request.logout();
        return "redirect:";
    }
}
