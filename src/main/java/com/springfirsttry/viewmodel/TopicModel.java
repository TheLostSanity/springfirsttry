package com.springfirsttry.viewmodel;

import com.springfirsttry.model.Forum;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class TopicModel {

    private Date created;

    @NotNull
    @Size(min = 2, max = 100, message = "Length must be in range of 2 to 100")
    public String topicName;

    private Forum topicForum;

    private Integer forumId;

    private String message;

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public Forum getTopicForum() {
        return topicForum;
    }

    public void setTopicForum(Forum forum) {
        this.topicForum = forum;
    }

    public Integer getForumId() {
        return forumId;
    }

    public void setForumId(Integer forumId) {
        this.forumId = forumId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
