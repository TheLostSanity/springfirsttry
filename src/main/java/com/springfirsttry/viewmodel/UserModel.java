package com.springfirsttry.viewmodel;

import javax.validation.constraints.Size;

public class UserModel {

    @Size(min=2, message = "lalala")
    private String username;

    @Size(min=2, message = "lalala")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
