package com.springfirsttry.viewmodel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ForumCategoryModel {
    @NotNull
    @Size(min = 2, message = "Category name&apos;s length must be more than 2")
    private String categoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
