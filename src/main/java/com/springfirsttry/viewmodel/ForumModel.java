package com.springfirsttry.viewmodel;

import com.springfirsttry.model.ForumCategory;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ForumModel {
    @NotNull
    @Size(min = 2, max = 100, message = "Length must be in range of 2 to 100")
    private String forumName;

    private String description;

    private ForumCategory forumCategory;

    private Integer categoryId;

//    private List<ForumCategory> categories;

    public String getForumName() {
        return forumName;
    }

    public void setForumName(String forumName) {
        this.forumName = forumName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ForumCategory getForumCategory() {
        return forumCategory;
    }

    public void setForumCategory(ForumCategory forumCategory) {
        this.forumCategory = forumCategory;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

   /* public List<ForumCategory> getCategories() {
        return categories;
    }*/

    /*public void setCategories(List<ForumCategory> categories) {
        this.categories = categories;
    }*/
}
