package com.springfirsttry.repository;

import com.springfirsttry.model.TopicMessage;
import com.springfirsttry.model.User;
import com.springfirsttry.viewmodel.TopicMessageModel;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicMessageRepositoryCustom {
    TopicMessage createAndSave(TopicMessageModel model, User user, Integer topicId);

    TopicMessage editAndSave(TopicMessage topicMessage, TopicMessageModel model);
}
