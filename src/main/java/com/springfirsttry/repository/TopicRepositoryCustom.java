package com.springfirsttry.repository;

import com.springfirsttry.model.Topic;
import com.springfirsttry.model.User;
import com.springfirsttry.viewmodel.TopicModel;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepositoryCustom {
    Topic createAndSave(TopicModel model, User user, Integer forumId);

    Topic editAndSave(Topic topic, TopicModel model);
}
