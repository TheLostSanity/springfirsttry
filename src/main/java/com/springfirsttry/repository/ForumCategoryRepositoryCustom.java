package com.springfirsttry.repository;

import com.springfirsttry.model.ForumCategory;
import com.springfirsttry.viewmodel.ForumCategoryModel;
import org.springframework.stereotype.Repository;

@Repository
public interface ForumCategoryRepositoryCustom {
    ForumCategory createAndSave(ForumCategoryModel model);

    ForumCategory editAndSave(ForumCategory category, ForumCategoryModel model);
}
