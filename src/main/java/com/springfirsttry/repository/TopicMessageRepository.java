package com.springfirsttry.repository;

import com.springfirsttry.model.TopicMessage;
import org.springframework.data.repository.CrudRepository;

public interface TopicMessageRepository extends CrudRepository<TopicMessage, Integer>, TopicMessageRepositoryCustom {
}
