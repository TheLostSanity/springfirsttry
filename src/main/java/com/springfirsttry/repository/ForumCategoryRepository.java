package com.springfirsttry.repository;

import com.springfirsttry.model.ForumCategory;
import org.springframework.data.repository.CrudRepository;

public interface ForumCategoryRepository extends CrudRepository<ForumCategory, Integer>, ForumCategoryRepositoryCustom {
}
