package com.springfirsttry.repository;

import com.springfirsttry.model.Topic;
import org.springframework.data.repository.CrudRepository;

public interface TopicRepository extends CrudRepository<Topic, Integer>, TopicRepositoryCustom {
}
