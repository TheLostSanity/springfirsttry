package com.springfirsttry.repository.impl;

import com.springfirsttry.model.Forum;
import com.springfirsttry.model.Topic;
import com.springfirsttry.model.User;
import com.springfirsttry.repository.ForumRepository;
import com.springfirsttry.repository.TopicRepository;
import com.springfirsttry.repository.TopicRepositoryCustom;
import com.springfirsttry.viewmodel.TopicModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

public class TopicRepositoryCustomImpl implements TopicRepositoryCustom {

    private final TopicRepository topicRepository;
    private final ForumRepository forumRepository;

    @Autowired
    @Lazy
    public TopicRepositoryCustomImpl(
            TopicRepository topicRepository,
            ForumRepository forumRepository
    ) {
        this.topicRepository = topicRepository;
        this.forumRepository = forumRepository;
    }

    @Override
    public Topic createAndSave(TopicModel model, User user, Integer forumId) {
        Date now = Date.from(Instant.now());
        Topic topic = new Topic();
        topic.setTopicName(model.getTopicName());
        topic.setCreated(now);
        Optional<Forum> forumOpt = forumRepository.findById(forumId);
        if (!forumOpt.isPresent()) {
            return null;
        }
        topic.setTopicForum(forumOpt.get());
        topic.setCreator(user);
        return topicRepository.save(topic);
    }

    @Override
    public Topic editAndSave(Topic topic, TopicModel model) {
        topic.setTopicName(model.getTopicName());
        return topicRepository.save(topic);
    }
}
