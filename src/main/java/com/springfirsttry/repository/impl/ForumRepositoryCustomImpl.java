package com.springfirsttry.repository.impl;

import com.springfirsttry.model.Forum;
import com.springfirsttry.model.ForumCategory;
import com.springfirsttry.repository.ForumCategoryRepository;
import com.springfirsttry.repository.ForumRepository;
import com.springfirsttry.repository.ForumRepositoryCustom;
import com.springfirsttry.viewmodel.ForumModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.util.Optional;

public class ForumRepositoryCustomImpl implements ForumRepositoryCustom {

    private final ForumCategoryRepository forumCategoryRepository;
    private final ForumRepository forumRepository;

    @Autowired
    @Lazy
    public ForumRepositoryCustomImpl(
            ForumCategoryRepository forumCategoryRepository,
            ForumRepository forumRepository
    ) {
        this.forumCategoryRepository = forumCategoryRepository;
        this.forumRepository = forumRepository;
    }

    @Override
    public Forum createAndSave(ForumModel model) {
        Optional<ForumCategory> optForumCategory = forumCategoryRepository.findById(model.getCategoryId());
        Forum forum = new Forum();
        forum.setForumName(model.getForumName());
        forum.setDescription(model.getDescription());
        forum.setForumCategory(optForumCategory.get());
        return forumRepository.save(forum);
    }

    @Override
    public Forum editAndSave(Forum forum, ForumModel model) {
        Optional<ForumCategory> forumCategory = forumCategoryRepository.findById(model.getCategoryId());
        forum.setForumName(model.getForumName());
        forum.setDescription(model.getDescription());
        forum.setForumCategory(forumCategory.get());
        return forumRepository.save(forum);
    }
}
