package com.springfirsttry.repository.impl;

import com.springfirsttry.model.Topic;
import com.springfirsttry.model.TopicMessage;
import com.springfirsttry.model.User;
import com.springfirsttry.repository.TopicMessageRepository;
import com.springfirsttry.repository.TopicMessageRepositoryCustom;
import com.springfirsttry.repository.TopicRepository;
import com.springfirsttry.viewmodel.TopicMessageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

public class TopicMessageRepositoryCustomImpl implements TopicMessageRepositoryCustom {

    private final TopicRepository topicRepository;
    private final TopicMessageRepository topicMessageRepository;

    @Autowired
    @Lazy
    public TopicMessageRepositoryCustomImpl(
            TopicRepository topicRepository,
            TopicMessageRepository topicMessageRepository
    ) {
        this.topicRepository = topicRepository;
        this.topicMessageRepository = topicMessageRepository;
    }

    @Override
    public TopicMessage createAndSave(TopicMessageModel model, User user, Integer topicId) {
        Optional<Topic> optTopic = topicRepository.findById(topicId);
        if (!optTopic.isPresent()) {
            return null;
        }
        Topic topic = optTopic.get();
        Date now = Date.from(Instant.now());
        TopicMessage topicMessage = new TopicMessage();
        topicMessage.setText(model.getText());
        topicMessage.setCreated(now);
        topicMessage.setModified(now);
        topicMessage.setMessageTopic(topic);
        topicMessage.setCreator(user);
        return topicMessageRepository.save(topicMessage);
    }

    @Override
    public TopicMessage editAndSave(TopicMessage topicMessage, TopicMessageModel model) {
        Date now = Date.from(Instant.now());
        topicMessage.setText(model.getText());
        topicMessage.setModified(now);
        return topicMessageRepository.save(topicMessage);
    }
}
