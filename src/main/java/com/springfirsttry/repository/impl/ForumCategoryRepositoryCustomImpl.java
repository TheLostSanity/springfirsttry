package com.springfirsttry.repository.impl;

import com.springfirsttry.model.ForumCategory;
import com.springfirsttry.repository.ForumCategoryRepository;
import com.springfirsttry.repository.ForumCategoryRepositoryCustom;
import com.springfirsttry.viewmodel.ForumCategoryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

public class ForumCategoryRepositoryCustomImpl implements ForumCategoryRepositoryCustom {

    private final ForumCategoryRepository forumCategoryRepository;

    @Autowired
    @Lazy
    public ForumCategoryRepositoryCustomImpl(ForumCategoryRepository forumCategoryRepository) {
        this.forumCategoryRepository = forumCategoryRepository;
    }

    @Override
    public ForumCategory createAndSave(ForumCategoryModel model) {
        ForumCategory forumCategory = new ForumCategory();
        forumCategory.setCategoryName(model.getCategoryName());
        return forumCategoryRepository.save(forumCategory);
    }

    @Override
    public ForumCategory editAndSave(ForumCategory category, ForumCategoryModel model) {
        category.setCategoryName(model.getCategoryName());
        return forumCategoryRepository.save(category);
    }
}
