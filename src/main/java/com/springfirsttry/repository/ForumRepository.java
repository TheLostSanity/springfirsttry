package com.springfirsttry.repository;

import com.springfirsttry.model.Forum;
import org.springframework.data.repository.CrudRepository;

public interface ForumRepository extends CrudRepository<Forum, Integer>, ForumRepositoryCustom {
}
