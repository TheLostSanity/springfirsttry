package com.springfirsttry.repository;

import com.springfirsttry.model.Forum;
import com.springfirsttry.viewmodel.ForumModel;
import org.springframework.stereotype.Repository;

@Repository
public interface ForumRepositoryCustom {
    Forum createAndSave(ForumModel model);

    Forum editAndSave(Forum forum, ForumModel model);
}
