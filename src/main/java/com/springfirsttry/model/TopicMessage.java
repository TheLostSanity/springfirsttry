package com.springfirsttry.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class TopicMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Basic
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Basic
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    @NotNull
    public String text;

    @ManyToOne
    @JoinColumn(name = "message_topic_id", referencedColumnName = "id")
    private Topic messageTopic;

    @ManyToOne
    @JoinColumn(name = "topic_message_creator_id", referencedColumnName = "id")
    private User creator;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Topic getMessageTopic() {
        return messageTopic;
    }

    public void setMessageTopic(Topic messageTopic) {
        this.messageTopic = messageTopic;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }
}
