package com.springfirsttry.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Basic
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @NotNull
    public String topicName;

    @ManyToOne
    @JoinColumn(name = "topic_forum_id", referencedColumnName = "id")
    private Forum topicForum;

    @OneToMany(
            mappedBy = "messageTopic",
            cascade = CascadeType.PERSIST
    )
    @org.hibernate.annotations.OrderBy(clause = "created ASC")
    private List<TopicMessage> messages = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "topic_creator_id", referencedColumnName = "id")
    private User creator;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public Forum getTopicForum() {
        return topicForum;
    }

    public void setTopicForum(Forum topicForum) {
        this.topicForum = topicForum;
    }

    public List<TopicMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<TopicMessage> messages) {
        this.messages = messages;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }
}
